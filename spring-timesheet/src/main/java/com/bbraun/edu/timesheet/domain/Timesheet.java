/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 19.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.domain;

public class Timesheet {

	private Employee	who;
	private Task		task;
	private Integer		hours;

	public Timesheet(Employee who, Task task, Integer hours) {

		this.who = who;
		this.task = task;
		this.hours = hours;
	}

	public Employee getWho() {

		return who;
	}

	public Task getTask() {

		return task;
	}

	public Integer getHours() {

		return hours;
	}

	/**
	* Manager can alter hours before closing task
	* @param hours New amount of hours
	*/
	public void alterHours(Integer hours) {

		this.hours = hours;
	}

	@Override
	public String toString() {

		return "Timesheet [who=" + who + ", task=" + task + ", hours=" + hours + "]";
	}
}
