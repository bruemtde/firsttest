/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 19.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.domain;


public class Employee {

	private String	name;
	private String	department;

	public Employee(String name, String department) {

		this.name = name;
		this.department = department;
	}

	public String getName() {

		return name;
	}

	public String getDepartment() {

		return department;
	}
}
