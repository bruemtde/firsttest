/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 19.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Task {

	private List<Employee>	assignedEmployees	= new ArrayList<Employee>();
	private Manager			manager;
	private boolean			completed;
	private String			description;

	public Task(String description, Manager manager, Employee... employees) {

		this.description = description;
		this.manager = manager;
		assignedEmployees.addAll(Arrays.asList(employees));
		completed = false;
	}

	public Manager getManager() {

		return manager;
	}

	public List<Employee> getAssignedEmployees() {

		return assignedEmployees;
	}

	public void addEmployee(Employee e) {

		assignedEmployees.add(e);
	}

	public void removeEmployee(Employee e) {

		assignedEmployees.remove(e);
	}

	public void completeTask() {

		completed = true;
	}
}
