/**
 * Copyright (c) 2013 BBM AG
 * Melsungen, Germany
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * BBM AG ("Confidential Information").  You shall not disclose such 
 * Confidential Information and shall use it only in accordance with 
 * the terms of the license agreement you entered into with BBM AG.
 * 
 * Created on: 19.09.2013
 * Last modified: $Date$
 * 
 * $HeadURL$
 * $Revision$
 */
package com.bbraun.edu.timesheet.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bbraun.edu.timesheet.domain.Employee;
import com.bbraun.edu.timesheet.service.GenericDao;
import com.bbraun.edu.timesheet.service.impl.InMemoryDao;

public class EmployeeDaoTest {

	private GenericDao<Employee, Long>	employeeDao	= new InMemoryDao<Employee, Long>();

	@Before
	public void setUp() {

		for (int i = 0; i < 5; i++) {
			Employee e = new Employee("Mike " + i, "IT");
			employeeDao.add(e);
		}
	}

	@Test
	public void testAdd() {

		int oldSize = employeeDao.list().size();
		Employee e = new Employee("Bob", "IT");
		employeeDao.add(e);
		int newSize = employeeDao.list().size();
		assertFalse(oldSize == newSize);
	}

	@Test
	public void testRemove() {

		int oldSize = employeeDao.list().size();
		Employee e = employeeDao.find(1L);
		employeeDao.remove(e);
		int newSize = employeeDao.list().size();
		assertFalse(oldSize == newSize);
	}

	@Test
	public void testUpdate() {

		//TODO: need real implementation
	}

	@Test
	public void testList() {

		List<Employee> list = employeeDao.list();
		assertNotNull(list);
		assertFalse(list.isEmpty());
	}

}
